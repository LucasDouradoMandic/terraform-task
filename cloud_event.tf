resource "aws_cloudwatch_event_rule" "every_day" {
  name                = "exec_lambda_fuctionlamb"
  description         = "Execute every day"
  schedule_expression = "rate(1 day)"
}
resource "aws_cloudwatch_event_target" "check_foo_every_day" {
  rule      = aws_cloudwatch_event_rule.every_day.name
  target_id = "lambda"
  arn       = aws_lambda_function.lambda.arn
}
resource "aws_cloudwatch_log_metric_filter" "metric_filter" {
  name           = var.function_name
  pattern        = "\"['sg-\""
  log_group_name = aws_cloudwatch_log_group.loggroup.name

  metric_transformation {
    name      = var.metric_filter_transformation_name
    namespace = "LogMetrics"
    value     = var.metric_filter_transformation_value
  }
}
resource "aws_cloudwatch_log_group" "loggroup" {
  name              = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
  retention_in_days = 14
}

resource "aws_cloudwatch_metric_alarm" "ports_alarm" {
  alarm_name          = "alarm-sg-ports-not-default"
  comparison_operator = var.metric_alarm_comparison_operator
  evaluation_periods  = var.metric_alarm_evaluation_periods
  threshold           = var.metric_alarm_threshold
  alarm_description   = <<EOT
    ->There is SG with ports in 0.0.0.0/0 open beyond 80 and 443. \\\
    ->Bucket link with log file: https://s3.console.aws.amazon.com/s3/buckets/${aws_s3_bucket.sg-ports.id}
    EOT
  metric_name         = aws_cloudwatch_log_metric_filter.metric_filter.metric_transformation[0].name
  namespace           = aws_cloudwatch_log_metric_filter.metric_filter.metric_transformation[0].namespace
  period              = var.metric_alarm_period
  statistic           = var.metric_alarm_statistic
}