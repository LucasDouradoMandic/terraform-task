#MAIN
variable "aws_region" {
  type = string
}

variable "locale_public_key" {
  type = string
}

#CLOUD_EVENT
variable "metric_filter_transformation_name" {
  type = string
}

variable "metric_filter_transformation_value" {
  type = string
}

variable "metric_alarm_evaluation_periods" {
  type = string
}

variable "metric_alarm_threshold" {
  type = string
}

variable "metric_alarm_comparison_operator" {
  type = string
}

variable "metric_alarm_period" {
  type = string
}

variable "metric_alarm_statistic" {
  type = string
}

#LAMBDA
variable "timeout" {
  type = string
}

variable "memory_size" {
  type = string
}

variable "runtime" {
  type = string
}

variable "principal_lambda" {
  type    = string
  default = "events.amazonaws.com"
}

variable "action_lambda" {
  type    = string
  default = "lambda:InvokeFunction"
}

variable "lambda_handler" {
  type    = string
  default = "function_lambda.lambda_handler"
}


variable "function_name" {
  type = string
}