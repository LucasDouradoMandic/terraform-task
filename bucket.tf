data "aws_canonical_user_id" "current_user" {}

resource "aws_s3_bucket" "sg-ports" {
  bucket = var.function_name
  versioning {
    enabled = true
  }
  grant {
    id          = data.aws_canonical_user_id.current_user.id
    type        = "CanonicalUser"
    permissions = ["FULL_CONTROL"]
  }

  grant {
    type        = "Group"
    permissions = ["READ", "WRITE"]
    uri         = "http://acs.amazonaws.com/groups/s3/LogDelivery"
  }
}
resource "aws_s3_bucket_public_access_block" "sg-ports" {
  bucket = aws_s3_bucket.sg-ports.id

  block_public_acls       = true
  ignore_public_acls      = true
  block_public_policy     = true
  restrict_public_buckets = true
}