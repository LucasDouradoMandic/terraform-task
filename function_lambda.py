import json
import boto3
import csv
import os

def lambda_handler(event, context):
    exposed_sg = []

    ec2client = boto3.client('ec2', 'us-east-1')
    response_all_security_groups = ec2client.describe_security_groups()

    FILE_NAME = "sg-ports-not-default.csv"
    LAMBDA_PATH = "/tmp/" + FILE_NAME
    BUCKET_NAME = os.environ['BUCKET']

    s3 = boto3.resource('s3')

    for sg in response_all_security_groups["SecurityGroups"]:
        for ip_ingress in sg["IpPermissions"]:
            if(ip_ingress["IpProtocol"] == "-1" or ip_ingress["IpProtocol"] == "all"):
                for ip_ranges in ip_ingress["IpRanges"]:
                    if (ip_ranges["CidrIp"] == "0.0.0.0/0"):
                        exposed_sg.append(sg["GroupId"])

            elif((ip_ingress["FromPort"] != 80 and ip_ingress["ToPort"] != 80) and (ip_ingress["FromPort"] != 443 and ip_ingress["ToPort"] != 443)):
                for ip_ranges in ip_ingress["IpRanges"]:
                    if (ip_ranges["CidrIp"] == "0.0.0.0/0"):
                        exposed_sg.append(sg["GroupId"])

    exposed_sg = list(dict.fromkeys(exposed_sg))

    with open(LAMBDA_PATH, 'w+', newline='') as f:
        w = csv.writer(f)
        for x in exposed_sg : w.writerow([x])
    
    s3.Bucket(BUCKET_NAME).upload_file(LAMBDA_PATH, FILE_NAME)

    if(len(exposed_sg) > 0):
        returnStatusCode = 200
    else:
        returnStatusCode = 404

    print(exposed_sg)

    return{
        "status_code": returnStatusCode,
        "body": exposed_sg
    }