provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_key_pair" "ssh_key_pair" {
  key_name   = "key_name"
  public_key = file(var.locale_public_key)
}