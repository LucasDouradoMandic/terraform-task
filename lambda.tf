provider "archive" {}
data "archive_file" "zip" {
  type        = "zip"
  source_file = "function_lambda.py"
  output_path = "function_lambda.zip"
}
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.lambda-sg-ports-not-default.name
  policy_arn = aws_iam_policy.policy.arn
}
resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = var.action_lambda
  function_name = aws_lambda_function.lambda.function_name
  principal     = var.principal_lambda
  source_arn    = aws_cloudwatch_event_rule.every_day.arn
}

resource "aws_lambda_function" "lambda" {
  function_name    = var.function_name
  filename         = data.archive_file.zip.output_path
  source_code_hash = data.archive_file.zip.output_base64sha256
  role             = aws_iam_role.lambda-sg-ports-not-default.arn
  handler          = var.lambda_handler
  runtime          = var.runtime
  memory_size      = var.memory_size
  timeout          = var.timeout

  environment {
    variables = {
      BUCKET = aws_s3_bucket.sg-ports.id,
      "foo"  = "bar"
    }
  }
}